import logging
from typing import List

from fuzzywuzzy import fuzz

from .voiceline import Voiceline


class MatchResult:
    def __init__(self, line, score):
        self.line = line
        self.score = score

    def __lt__(self, other):
        return self.score < other.score

    def __le__(self, other):
        return self.score <= other.score


class Matcher:
    def __init__(self, lines: List[Voiceline]):
        self.lines = lines
        self.logger = logging.getLogger('app.matcher')
        self.logger.info(f"Created a {self.__class__.__name__} with {len(lines)} line(s).")
        self.tag_map = self.make_tag_map()

    def make_tag_map(self):
        self.logger.info("Preparing tag map...")

        tag_map = dict()
        for line in self.lines:
            for tag in line.tags:
                if tag not in tag_map:
                    tag_map[tag] = set()
                tag_map[tag].add(line)
        
        self.logger.info(f"Tag map created with {len(tag_map.keys())} tags and {sum(len(s) for s in tag_map.values())} entries.")
        return tag_map


    @staticmethod
    def match_score(query, line, weights: List[float] = None):
        if weights is None or len(weights) < 2:
            weights = [0.6, 0.4]

        return weights[0] * fuzz.partial_token_sort_ratio(query, line.text) \
            + weights[1] * fuzz.token_sort_ratio(query, line.text)

    def _find(self, query, min_score):
        self.logger.debug(f"Starting the search for '{query}'...")
        tags = []
        if '+' in query:
            # extract all tags and rebuild query without them
            tokens = query.split(' ')
            query = ''
            for token in tokens:
                if token.startswith('+'):
                    tags.append(token[1:].lower())
                else:
                    query += token
            self.logger.debug(f"Extracted {len(tags)} tags ({tags}); new query: '{query}'")
        
        if len(tags) > 0 and any(tag in self.tag_map for tag in tags):
            lookup_set = set.intersection(*[
                self.tag_map[tag]
                for tag in tags
                if tag in self.tag_map
            ])
        else:
            lookup_set = self.lines
        self.logger.debug(f"Lookup set has {len(lookup_set)} lines.")

        results = []
        for line in lookup_set:
            score = self.match_score(query, line)
            if score >= min_score:
                results.append(MatchResult(line, score))
                self.logger.debug(f"[{score:>5.1f}] '{line.text}'")
        results.sort(reverse=True)
        if len(results) == 0:
            self.logger.debug("Nothing found.")
        return results

    def find(self, query: str, min_score: float = 60, max_entries: int = 3):
        if len(query) > 200:
            self.logger.warning(f"Query '{query[:50]}...' was discarded because it is too long ({len(query)}).")
            return []
        self.logger.info(f"Query: '{query}', max_entries: {max_entries}")
        return [r.line for r in self._find(query, min_score)[:max_entries]]

    def find_one(self, query: str, min_score: float = 60, score_diff: float = 10):
        self.logger.info(f"Query: '{query}', one result.")
        results = self._find(query, min_score - score_diff - 1)[:2]
        too_good_threshold = (100.0 + min_score) / 2

        if len(results) == 0:
            return None
        elif len(results) == 1:
            return results[0].line
        elif (results[0].score - results[1].score >= score_diff) or (results[0].score >= too_good_threshold):
            return results[0].line
        else:
            return None
