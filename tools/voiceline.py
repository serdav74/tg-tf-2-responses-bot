from hashlib import md5
from pathlib import Path
from typing import List

import requests


def download(url: str, path: Path):
    r = requests.get(url)
    path.touch()
    path.write_bytes(r.content)


class Voiceline:
    def __init__(self, text: str, url: str, tags: List[str]):
        self.text = text
        self.url = url
        self.tags = tags
        self.local_file = None
        self.cache_id = None

    def download(self, folder: Path):
        filename = folder / (self.url.split('/')[-1])
        if not filename.exists():
            download(self.url, filename)
        self.local_file = filename
    
    def static_hash(self):
        return md5(bytes(self.url, encoding="utf-8")).hexdigest()

    @classmethod
    def load(cls, definition):
        return cls(
            text=definition['text'],
            url=definition['audio'],
            tags=definition['tags']
        )

    @classmethod
    def load_batch(cls, data):
        return list(
            cls.load(item) for item in data
        )
