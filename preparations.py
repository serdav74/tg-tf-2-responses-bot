import json
import logging
import sys
from pathlib import Path
from time import sleep
from typing import List, Union

from telebot import TeleBot, logger as telebot_logger

from tools.voiceline import Voiceline

TG_CACHE_FILENAME = 'tg_cache.json'

logger = logging.getLogger('app.prep')


def load_config(filename: Union[str, Path]):
    logger.debug(f"Reading: {filename}")
    with open(str(filename), 'rt') as f:
        return json.load(f)


def init_tg_cache(
        bot: TeleBot,
        chat_id: str,
        cache_folder: Path,
        lines: List[Voiceline],
        sleep_interval: float = 3.0,
        ignore_existing: bool = False
):
    logger.info(f"Initializing cache: {len(lines)} lines to upload, estimated time: {len(lines) * sleep_interval:.1f}s")
    cache = dict()

    cache_file = cache_folder / TG_CACHE_FILENAME

    try:
        if cache_file.exists() and not ignore_existing:
            cache = load_config(cache_file)
            logger.info(f"Successfully loaded existing cache with {len(cache)} entries.")
    except json.JSONDecodeError:
        logger.warning("Found existing cache but failed to decode JSON, skipping.")
        pass

    for line in lines:
        if line.static_hash() in cache:
            continue

        if line.local_file is None:
            logger.debug(f"Downloading {line.url}")
            line.download(folder=cache_folder)

        logger.info(f"Uploading {line.local_file}")
        with open(line.local_file, 'rb') as f:
            sent = bot.send_voice(
                chat_id,
                f
            )
            line.cache_id = cache[line.static_hash()] = sent.voice.file_id
        sleep(sleep_interval)

    with open(str(cache_file), 'wt') as f:
        json.dump(cache, f)


def load_tg_cache(cache_folder: Path, lines: List[Voiceline]):
    cache_file = cache_folder / TG_CACHE_FILENAME
    if not cache_file.exists():
        raise FileNotFoundError('cache_folder doesn\'t have a valid cache')

    cache = load_config(cache_file)

    logger.info(f"Loading cache: {len(lines)} lines, {len(cache)} cache entries.")

    fully_loaded = True

    for line in lines:
        try:
            line.cache_id = cache[line.static_hash()]
        except KeyError:
            logger.warning(f"Line '{line.text[:30]}' doesn't have matching cache entries!")
            fully_loaded = False

    return fully_loaded


def setup_logging(level=logging.INFO):
    logger = logging.getLogger()
    logger.handlers.clear()
    telebot_logger.handlers.clear()

    handler = logging.StreamHandler(stream=sys.stdout)
    formatter = logging.Formatter(
        fmt="{asctime:>16} | {name:16} | {levelname:8} | {message}",
        style="{"
    )

    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger
