# tg-dd-responses-bot

## Installation

You will need Python>=3.6 for this.

* Set up your bot with `@BotFather` bot (and enable Inline mode)
* Clone this
* Install `python3-dev` and `build-essential` with your package manager
* You might want to create a virtual environment
* `pip install -r requirements.txt`
* `cp config.example.json config.json` and fill the latter with your values
* Make sure you can write to this directory and have some free space
* Run `app.py`

The bot will have to cache *all* the voice lines and it is going to take `340 * 3s = a lot` of time. This has to be done once per bot account (or each time you delete `tg_cache.json`)
