import logging
from pathlib import Path

from bot import AncestorBot
from preparations import init_tg_cache, load_tg_cache, load_config, setup_logging
from tools.matcher import Matcher
from tools.voiceline import Voiceline

if __name__ == '__main__':
    setup_logging("INFO")
    logger = logging.getLogger('app')

    config = load_config('config.json')

    lines_config = load_config(config['VOICELINES']['CONFIG_PATH'])['voice-lines']
    cache_path = Path(config['VOICELINES']['CACHE_FOLDER'])
    if not cache_path.exists():
        cache_path.mkdir()
        logger.info(f"Created cache folder: {cache_path}")

    lines = Voiceline.load_batch(lines_config)
    matcher = Matcher(lines)

    bot = AncestorBot(matcher=matcher, token=config['API_TOKEN'], threaded=False)

    try:
        success = load_tg_cache(cache_folder=cache_path, lines=lines)
        if not success:
            logger.warning("Some lines weren't found in cache.")
            raise ValueError
    except (FileNotFoundError, ValueError):
        init_tg_cache(bot=bot, chat_id=config['DUMPSTER_CHAT_ID'], cache_folder=cache_path, lines=lines)

    bot.favourite_lines.extend([
        matcher.find_one("Nice hustle, tons of fun! Next time eat a salad"),
        matcher.find_one("Wave goodbye to you head, wanker"),
        matcher.find_one("gibberish")
    ])

    logger.info("Everything's ready.")
    bot.infinity_polling(none_stop=True)
