from time import sleep

from requests.exceptions import Timeout, ConnectionError
from telebot import TeleBot, types, apihelper, logger as telebot_logger


class AncestorBot(TeleBot):
    def __init__(self, matcher, *args, **kwargs):
        apihelper.READ_TIMEOUT = 5
        self.matcher = matcher
        self.favourite_lines = []
        super().__init__(*args, **kwargs)
        telebot_logger.setLevel("INFO")
        self.register_custom_handlers()

    def infinity_polling(self, timeout=5, *args, **kwargs):
        while not self._TeleBot__stop_polling.is_set():
            try:
                self.polling(timeout=timeout, *args, **kwargs)
            except KeyboardInterrupt:
                self.stop_polling()
            except (Timeout, ConnectionError):
                sleep(timeout)
                pass
        telebot_logger.info("Infinite polling stopped.")

    def send_reply(self, message, text, reply_markup=None, reply=False, **kwargs):
        """
        Отправляет ответ на сообщение.
        :param message: Исходное сообщение, на которое надо ответить
        :param text: Текст ответа
        :param reply_markup: Разметка для ответа (например, с кнопками)
        :param reply: Пересылать ли исходное сообщение
        :param kwargs: Прочие параметры
        :return: Ответ от API
        """
        chat_id = message.chat.id
        tries = 2
        while tries > 0:
            tries -= 1
            try:
                return self.send_message(
                    chat_id,
                    text,
                    reply_markup=reply_markup,
                    reply_to_message_id=(message.message_id if reply else None),
                    **kwargs
                )
            except Exception as e:
                telebot_logger.error("An error has occured.", exc_info=e)
                sleep(1)
                continue

    def register_custom_handlers(self):
        @self.message_handler(commands=['start', 'hi', 'help'])
        def hi(message):
            self.send_reply(
                message,
                'Just send me at least half of the quote and I\'ll respond with a voice message.\n\n'
                'You can also use me as an inline bot: use `@tf2sounds_bot query` in any chat.',
                reply=True,
                parse_mode='markdown'
            )

        @self.message_handler(regexp='(?i)^[^/]')
        def query(message):
            line = self.matcher.find_one(message.text)
            if line is None:
                if message.chat.type != 'group':
                    return self.send_reply(message, 'No matches found.')
            else:
                file_id = line.cache_id
                self.send_voice(message.chat.id, file_id, caption=line.text)

        @self.message_handler(commands=['s'])
        def query_cmd(message):
            line = self.matcher.find_one(' '.join(message.text.split()[1:]))
            if line is None:
                return self.send_reply(message, 'No matches found')
            else:
                file_id = line.cache_id
                self.send_voice(message.chat.id, file_id, caption=line.text)

        @self.inline_handler(lambda q: True)
        def inline_query(query):
            try:
                lines = []
                if len(query.query) > 0:
                    lines.extend(self.matcher.find(query.query, max_entries=6))
                else:
                    lines.extend(self.favourite_lines)

                results = []
                for line in lines:
                    if line:
                        if line.cache_id:
                            results.append(
                                types.InlineQueryResultCachedVoice(
                                    id=line.static_hash(),
                                    voice_file_id=line.cache_id,
                                    title=line.text,
                                    caption=line.text
                                )
                            )
                        else:
                            # last resort, doesn't work correctly
                            results.append(
                                types.InlineQueryResultVoice(
                                    id=line.static_hash(),
                                    voice_url=line.url,
                                    title=line.text,
                                    caption=line.text
                                )
                            )
                self.answer_inline_query(
                    query.id,
                    results,
                    cache_time=300
                )

            except Exception as e:
                telebot_logger.error(
                    "An error has occured when trying to respond to InlineQuery.",
                    exc_info=e
                )
